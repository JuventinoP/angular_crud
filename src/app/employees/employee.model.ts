export interface Employee {
  id?: number;
  name: string;
  age: number;
  experience: string;
  department: string; 
  
}
