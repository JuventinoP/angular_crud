import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store, State, select } from "@ngrx/store";

import * as employeeActions from "../state/employee.actions";
import * as fromEmployee from "../state/employee.reducer";
import { Employee } from "../employee.model";

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.css']
})
export class EmployeeAddComponent implements OnInit {

  employeeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromEmployee.AppState>
  ) {}

  ngOnInit() {
    this.employeeForm = this.fb.group({
      name: ["", Validators.required],
      age: ["", Validators.required],
      experience: ["", Validators.required],
      department: ["", Validators.required]
    });
  }


  createEmployee() {
    const newEmployee: Employee = {
      name: this.employeeForm.get("name").value,
      age: this.employeeForm.get("age").value,
      experience: this.employeeForm.get("experience").value,
      department: this.employeeForm.get("department").value
    };

    this.store.dispatch(new employeeActions.CreateEmployee(newEmployee));

    this.employeeForm.reset();
  }
}
