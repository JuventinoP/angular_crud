import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee/employee.component';
import { EmployeeAddComponent } from './employee-add/employee-add.component';
import { EmployeeEditComponent } from './employee-edit/employee-edit.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { RouterModule, Routes } from "@angular/router";
import { EffectsModule, Actions } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

import { employeeReducer } from "./state/employee.reducer";
import { EmployeeEffect } from "./state/employee.effects";



const employeeRoutes: Routes = [{ path: "", component: EmployeeComponent }];

@NgModule({
  imports:   [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(employeeRoutes),
    StoreModule.forFeature("employees", employeeReducer),
    EffectsModule.forFeature([EmployeeEffect])
  ],
  declarations: [EmployeeComponent, EmployeeAddComponent, EmployeeEditComponent, EmployeeListComponent]
})
export class EmployeesModule { }





