
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";

import { Observable } from "rxjs";

import * as employeeActions from "../state/employee.actions";
import * as fromEmployee from "../state/employee.reducer";
import { Employee } from "../employee.model";

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {

  employeeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<fromEmployee.AppState>
  ) { }

  ngOnInit() {
    this.employeeForm = this.fb.group({
      name: ["", Validators.required],
      age: ["", Validators.required],
      experience: ["", Validators.required],
      department: ["", Validators.required],
      id: null
    })

    const employee$: Observable<Employee> = this.store.select(
      fromEmployee.getCurrentEmployee
    )

    employee$.subscribe(currentEmployee => {
      if (currentEmployee) {
        this.employeeForm.patchValue({
          name: currentEmployee.name,
          age: currentEmployee.age,
          experience: currentEmployee.experience,
          department: currentEmployee.department,
          id: currentEmployee.id
        });
      }
    })
  }

  updateEmployee() {
    const updatedEmployee: Employee = {
      name: this.employeeForm.get("name").value,
      age: this.employeeForm.get("age").value,
      experience: this.employeeForm.get("experience").value,
      department: this.employeeForm.get("department").value,
      id: this.employeeForm.get("id").value
    };

    this.store.dispatch(new employeeActions.UpdateEmployee(updatedEmployee))
  }

}
