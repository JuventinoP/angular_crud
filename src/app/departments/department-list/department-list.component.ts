import { Component, OnInit } from '@angular/core';

import { Store, select } from "@ngrx/store";
import { Observable } from "rxjs";

import * as departmentActions from "../state/department.actions";
import * as fromDepartment from "../state/department.reducer";
import { Department } from "../department.model";

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {

  departments$: Observable<Department[]>;
  error$: Observable<String>;

  constructor(private store: Store<fromDepartment.AppState>) { }

  ngOnInit() {
    this.store.dispatch(new departmentActions.LoadDepartments());
    this.departments$ = this.store.pipe(select(fromDepartment.getDepartments));
    this.error$ = this.store.pipe(select(fromDepartment.getError));
  }

  deleteDepartment(department: Department) {
    if (confirm("Are You Sure You Want To Delete The Department?")) {
      this.store.dispatch(new departmentActions.DeleteDepartment(department.id));
    }
  }

  editDepartment(department: Department) {
    this.store.dispatch(new departmentActions.LoadDepartment(department.id));
  }

}
