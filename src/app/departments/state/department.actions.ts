import { Action } from '@ngrx/store';

import { Update } from '@ngrx/entity';

import { Department } from '../department.model';

export enum DepartmentActionTypes {
    LOAD_DEPARTMENTS = '[Department] Load Departments',
    LOAD_DEPARTMENTS_SUCCESS = '[Department] Load Departments Success',
    LOAD_DEPARTMENTS_FAIL = '[Department] Load Departments Fail',
    LOAD_DEPARTMENT = '[Department] Load Department',
    LOAD_DEPARTMENT_SUCCESS = '[Department] Load Department Success',
    LOAD_DEPARTMENT_FAIL = '[Department] Load Department Fail',
    CREATE_DEPARTMENT = '[Department] Create Department',
    CREATE_DEPARTMENT_SUCCESS = '[Department] Create Department Success',
    CREATE_DEPARTMENT_FAIL = '[Department] Create Department Fail',
    UPDATE_DEPARTMENT = '[Department] Update Department',
    UPDATE_DEPARTMENT_SUCCESS = '[Department] Update Department Success',
    UPDATE_DEPARTMENT_FAIL = '[Department] Update Department Fail',
    DELETE_DEPARTMENT = '[Department] Delete Department',
    DELETE_DEPARTMENT_SUCCESS = '[Department] Delete Department Success',
    DELETE_DEPARTMENT_FAIL = '[Department] Delete Department Fail'
}

export class LoadDepartments implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENTS;
}

export class LoadDepartmentsSuccess implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENTS_SUCCESS;

    constructor(public payload: Department[]) {}
}

export class LoadDepartmentsFail implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENTS_FAIL;

    constructor(public payload: string) {}
}

export class LoadDepartment implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENT;

    constructor(public payload: number) {}
}

export class LoadDepartmentSuccess implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENT_SUCCESS;

    constructor(public payload: Department) {}
}

export class LoadDepartmentFail implements Action {
    readonly type = DepartmentActionTypes.LOAD_DEPARTMENT_FAIL;

    constructor(public payload: string) {}
}

export class CreateDepartment implements Action {
    readonly type = DepartmentActionTypes.CREATE_DEPARTMENT;

    constructor(public payload: Department) {}
}

export class CreateDepartmentSuccess implements Action {
    readonly type = DepartmentActionTypes.CREATE_DEPARTMENT_SUCCESS;

    constructor(public payload: Department) {}
}

export class CreateDepartmentFail implements Action {
    readonly type = DepartmentActionTypes.CREATE_DEPARTMENT_FAIL;

    constructor(public payload: string) {}
}

export class UpdateDepartment implements Action {
    readonly type = DepartmentActionTypes.UPDATE_DEPARTMENT;

    constructor(public payload: Department) {}
}

export class UpdateDepartmentSuccess implements Action {
    readonly type = DepartmentActionTypes.UPDATE_DEPARTMENT_SUCCESS;

    constructor(public payload: Update<Department>) {}
}

export class UpdateDepartmentFail implements Action {
    readonly type = DepartmentActionTypes.UPDATE_DEPARTMENT_FAIL;

    constructor(public payload: string) {}
}

export class DeleteDepartment implements Action {
    readonly type = DepartmentActionTypes.DELETE_DEPARTMENT;

    constructor(public payload: number) {}
}

export class DeleteDepartmentSuccess implements Action {
    readonly type = DepartmentActionTypes.DELETE_DEPARTMENT_SUCCESS;

    constructor(public payload: number) {}
}

export class DeleteDepartmentFail implements Action {
    readonly type = DepartmentActionTypes.DELETE_DEPARTMENT_FAIL;

    constructor(public payload: string) {}
}

export type Action =
    | LoadDepartments
    | LoadDepartmentsSuccess
    | LoadDepartmentsFail
    | LoadDepartment
    | LoadDepartmentSuccess
    | LoadDepartmentFail
    | CreateDepartment
    | CreateDepartmentSuccess
    | CreateDepartmentFail
    | UpdateDepartment
    | UpdateDepartmentSuccess
    | UpdateDepartmentFail
    | DeleteDepartment
    | DeleteDepartmentSuccess
    | DeleteDepartmentFail;