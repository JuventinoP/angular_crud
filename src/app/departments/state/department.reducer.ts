import * as departmentActions from "./department.actions";
import { createFeatureSelector, createSelector } from "@ngrx/store";

import { EntityState, EntityAdapter, createEntityAdapter } from "@ngrx/entity";

import { Department } from "../department.model";
import * as fromRoot from "../../state/app-state";

export interface DepartmentState extends EntityState<Department> {
  selectedDepartmentId: number | null;
  loading: boolean;
  loaded: boolean;
  error: string;
}

export interface AppState extends fromRoot.AppState {
    departments: DepartmentState;
}

export const departmentAdapter: EntityAdapter<Department> = createEntityAdapter<Department>();

export const defaultDepartment: DepartmentState = {
  ids: [],
  entities: {},
  selectedDepartmentId: null,
  loading: false,
  loaded: false,
  error: ""
};

export const initialState = departmentAdapter.getInitialState(defaultDepartment);

export function departmentReducer(
  state = initialState,
  action: departmentActions.Action
): DepartmentState {
  switch (action.type) {
    case departmentActions.DepartmentActionTypes.LOAD_DEPARTMENTS_SUCCESS: {
      return departmentAdapter.addAll(action.payload, {
        ...state,
        loading: false,
        loaded: true
      });
    }
    case departmentActions.DepartmentActionTypes.LOAD_DEPARTMENTS_FAIL: {
      return {
        ...state,
        entities: {},
        loading: false,
        loaded: false,
        error: action.payload
      };
    }

    case departmentActions.DepartmentActionTypes.LOAD_DEPARTMENT_SUCCESS: {
      return departmentAdapter.addOne(action.payload, {
        ...state,
        selectedDepartmentId: action.payload.id
      });
    }
    case departmentActions.DepartmentActionTypes.LOAD_DEPARTMENT_FAIL: {
      return {
        ...state,
        error: action.payload
      };
    }

    case departmentActions.DepartmentActionTypes.CREATE_DEPARTMENT_SUCCESS: {
      return departmentAdapter.addOne(action.payload, state);
    }
    case departmentActions.DepartmentActionTypes.CREATE_DEPARTMENT_FAIL: {
      return {
        ...state,
        error: action.payload
      };
    }

    case departmentActions.DepartmentActionTypes.UPDATE_DEPARTMENT_SUCCESS: {
      return departmentAdapter.updateOne(action.payload, state);
    }
    case departmentActions.DepartmentActionTypes.UPDATE_DEPARTMENT_FAIL: {
      return {
        ...state,
        error: action.payload
      };
    }

    case departmentActions.DepartmentActionTypes.DELETE_DEPARTMENT_SUCCESS: {
      return departmentAdapter.removeOne(action.payload, state);
    }
    case departmentActions.DepartmentActionTypes.DELETE_DEPARTMENT_FAIL: {
      return {
        ...state,
        error: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

const getDepartmentFeatureState = createFeatureSelector<DepartmentState>(
  "departments"
);

export const getDepartments = createSelector(
  getDepartmentFeatureState,
  departmentAdapter.getSelectors().selectAll
);

export const getDepartmentsLoading = createSelector(
  getDepartmentFeatureState,
  (state: DepartmentState) => state.loading
);

export const getDepartmentssLoaded = createSelector(
  getDepartmentFeatureState,
  (state: DepartmentState) => state.loaded
);

export const getError = createSelector(
  getDepartmentFeatureState,
  (state: DepartmentState) => state.error
);

export const getCurrentDepartmentId = createSelector(
  getDepartmentFeatureState,
  (state: DepartmentState) => state.selectedDepartmentId
);
export const getCurrentDepartment = createSelector(
  getDepartmentFeatureState,
  getCurrentDepartmentId,
  state => state.entities[state.selectedDepartmentId]
);