import { Injectable } from "@angular/core";

import { Actions, Effect, ofType } from "@ngrx/effects";
import { Action } from "@ngrx/store";

import { Observable, of } from "rxjs";
import { map, mergeMap, catchError } from "rxjs/operators";

import { DepartmentService } from "../department.service";
import * as departmentActions from "../state/department.actions";
import { Department } from "../department.model";

@Injectable()
export class DepartmentEffect {
  constructor(
    private actions$: Actions,
    private departmentService: DepartmentService
  ) {}

  @Effect()
  loadDepartments$: Observable<Action> = this.actions$.pipe(
    ofType<departmentActions.LoadDepartments>(
      departmentActions.DepartmentActionTypes.LOAD_DEPARTMENTS
    ),
    mergeMap((action: departmentActions.LoadDepartments) =>
      this.departmentService.getDepartments().pipe(
        map(
          (departments: Department[]) =>
            new departmentActions.LoadDepartmentsSuccess(departments)
        ),
        catchError(err => of(new departmentActions.LoadDepartmentsFail(err)))
      )
    )
  );

  @Effect()
  loadDepartment$: Observable<Action> = this.actions$.pipe(
    ofType<departmentActions.LoadDepartment>(
      departmentActions.DepartmentActionTypes.LOAD_DEPARTMENT
    ),
    mergeMap((action: departmentActions.LoadDepartment) =>
      this.departmentService.getDepartmentById(action.payload).pipe(
        map(
          (department: Department) =>
            new departmentActions.LoadDepartmentSuccess(department)
        ),
        catchError(err => of(new departmentActions.LoadDepartmentFail(err)))
      )
    )
  );

  @Effect()
  createDepartment$: Observable<Action> = this.actions$.pipe(
    ofType<departmentActions.CreateDepartment>(
      departmentActions.DepartmentActionTypes.CREATE_DEPARTMENT
    ),
    map((action: departmentActions.CreateDepartment) => action.payload),
    mergeMap((department: Department) =>
      this.departmentService.createDepartment(department).pipe(
        map(
          (newDepartment: Department) =>
            new departmentActions.CreateDepartmentSuccess(newDepartment)
        ),
        catchError(err => of(new departmentActions.CreateDepartmentFail(err)))
      )
    )
  );

  @Effect()
  updateDepartment$: Observable<Action> = this.actions$.pipe(
    ofType<departmentActions.UpdateDepartment>(
      departmentActions.DepartmentActionTypes.UPDATE_DEPARTMENT
    ),
    map((action: departmentActions.UpdateDepartment) => action.payload),
    mergeMap((department: Department) =>
      this.departmentService.updateDepartment(department).pipe(
        map(
          (updateDepartment: Department) =>
            new departmentActions.UpdateDepartmentSuccess({
              id: updateDepartment.id,
              changes: updateDepartment
            })
        ),
        catchError(err => of(new departmentActions.UpdateDepartmentFail(err)))
      )
    )
  );

  @Effect()
  deleteDepartment$: Observable<Action> = this.actions$.pipe(
    ofType<departmentActions.DeleteDepartment>(
      departmentActions.DepartmentActionTypes.DELETE_DEPARTMENT
    ),
    map((action: departmentActions.DeleteDepartment) => action.payload),
    mergeMap((id: number) =>
      this.departmentService.deleteDepartment(id).pipe(
        map(() => new departmentActions.DeleteDepartmentSuccess(id)),
        catchError(err => of(new departmentActions.DeleteDepartmentFail(err)))
      )
    )
  );

}