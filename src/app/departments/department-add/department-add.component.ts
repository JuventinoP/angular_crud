import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store, State, select } from "@ngrx/store";
import * as departmentActions from "../state/department.actions";
import * as fromDepartment from "../state/department.reducer";
import { Department } from "../department.model";

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {

  departmentForm: FormGroup;

  constructor(private fb: FormBuilder, private store: Store<fromDepartment.AppState>) { }

  ngOnInit() {
    this.departmentForm = this.fb.group({
      name: ["", Validators.required]
    });
  }

  createDepartment() {
    const newDepartment: Department = {
      name: this.departmentForm.get("name").value,
    };

    this.store.dispatch(new departmentActions.CreateDepartment(newDepartment));

    this.departmentForm.reset();
  }

}
