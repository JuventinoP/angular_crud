import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Store } from "@ngrx/store";

import { Observable } from "rxjs";

import * as departmentActions from "../state/department.actions";
import * as fromDepartment from "../state/department.reducer";
import { Department } from "../department.model";

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})
export class DepartmentEditComponent implements OnInit {

  departmentForm: FormGroup;

  constructor(private fb: FormBuilder, private store: Store<fromDepartment.AppState>) { }

  ngOnInit() {
    this.departmentForm = this.fb.group({
      name: ["", Validators.required],
      id: null
    });

    const department$: Observable<Department> = this.store.select(
      fromDepartment.getCurrentDepartment
    )

    department$.subscribe(currentDepartment => {
      if (currentDepartment) {
        this.departmentForm.patchValue({
          name: currentDepartment.name,
          id: currentDepartment.id
        });
      }
    });
  }

  updateDepartment() {
    const updatedDepartment: Department = {
      name: this.departmentForm.get("name").value,
      id: this.departmentForm.get("id").value
    };

    this.store.dispatch(new departmentActions.UpdateDepartment(updatedDepartment))
  }

}
