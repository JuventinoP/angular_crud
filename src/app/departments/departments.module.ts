import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { EffectsModule, Actions } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { departmentReducer } from './state/department.reducer';

import { DepartmentComponent } from './department/department.component';
import { DepartmentAddComponent } from './department-add/department-add.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentEffect } from './state/department.effects';

const departmentRoutes: Routes = [{ path: "", component: DepartmentComponent}]

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forChild(departmentRoutes),
    StoreModule.forFeature("departments", departmentReducer),
    EffectsModule.forFeature([DepartmentEffect])
  ],
  declarations: [
    DepartmentComponent,
    DepartmentAddComponent,
    DepartmentEditComponent,
    DepartmentListComponent
  ]
})
export class DepartmentsModule { }
