import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { Department } from './department.model';
import { Customer } from '../customers/customer.model';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  
  private departmentsUrl = 'http://localhost:3000/departments';

  constructor(private http: HttpClient) { }

  getDepartments(): Observable<Department[]> {
    return this.http.get<Department[]>(this.departmentsUrl);
  }

  getDepartmentById(payload: number): Observable<Department> {
    return this.http.get<Customer>(`${ this.departmentsUrl }/${ payload }`);
  }

  createDepartment(payload: Department): Observable<Department> {
    return this.http.post<Department>(this.departmentsUrl, payload);
  }

  updateDepartment(department: Department): Observable<Department> {
    return this.http.patch<Department>(`${ this.departmentsUrl }/${ department.id }`, department);
  }

  deleteDepartment(payload: number) {
    return this.http.delete(`${ this.departmentsUrl }/${ payload }`);
  }
}
